class N {

  constructor(node, obj) {
    if (node) {
      return this.createNodes(parser.parse(node), obj);
    }
  }
  createNode(node, variables) {
    var node = document.createElement(node.tagName);
    //observer.observe(target, {attributeOldValue : true, attributes: true, childList: true, characterData: true });
    return node;
  }

  createNodes(Dom, obj) {
    let self = this;
    let nodes = [];
    for (var i = 0; i < Dom.length; i++) {
      if (window[Dom[i].tagName]) {
        obj = window[Dom[i].tagName];
      }
      nodes[i] = self.createNode(Dom[i]);
      nodes[i].setAttribute('data-tinycomp-id', obj._id + "-" + i);
      if (Dom[i].attributes) {
        Dom[i].attributes.forEach((e, k) => {
          if (e != null) {
            nodes[i].setAttribute(e.field, e.value);
            if (e.variable) {
              nodes[i]["_" + e.field] = "{" + e.variable + "}";
              let type = e.field.substr(0, 2) == "on" && e.field.match(/[-_]/) == null ? "event" : "attribute";

              obj._variables.push([e.variable, type, obj._id + "-" + i, e.field]);
            }
          }
        });
      }
      nodes[i].textContent = Dom[i].textContent;
      nodes[i]._textContent = Dom[i].textContent;
      [].concat((Dom[i].textContent).match(/\{.+?[^\\\}]\}/ig) || []).forEach((e, k) => {
        obj._variables.push([e.slice(0, -1).slice(1), "textContent", obj._id + "-" + i]);
      })

      if (Dom[i].variable) {
        obj._variables.push([Dom[i].variable, "content", obj._id + "-" + i + "-content-test"]);
      }
      if (window[Dom[i].tagName]) {

        window[Dom[i].tagName].DOMNode = nodes[i];
        nodes[i] = window[Dom[i].tagName];
      }
      obj._id += "-" + i
      self.createNodes((Dom[i].children != null ? Dom[i].children : []), obj).forEach((e) => {
        nodes[i].appendChild(e);
      });

    }
    return nodes;
  }

  N(string, obj) {
    return this.createNodes(parser.parse(string), obj);
  }
}


class Component {
  constructor(obj) {
    this.N = new N().N;
    this.DOMNode; // = document.createElement(obj.Name);
    this.Name = obj.Name;
    this._variables = [];
    this.component = true;
    for (var i in obj) {
      this[i] = obj[i];
    }
    this.listeners = [];
    this.state = this._state = this.getInitialState();
    console.log(this.state);
    this.createComponent(obj.template);
    this.render();
    let self = this;
    setTimeout(function() {
        self.render();
    }, 0);
  }

  getInitialState() {
    return {};
  }

  setState(obj) {
    let obj_ = obj || this.getInitialState();
    this._state = this.state = Object.assign(this._state, obj_);
    this.render();
    return;
  }

  createComponent(node_) {
    let node;
    let self = this;
    if (typeof node_ == "string") {
      node = new N(node_, self);
    }
    this.Nodes = node;
    return;
  }

  render() {
    let self = this;
    this._variables.forEach((e) => {
      let domnode = document.querySelector('[data-tinycomp-id="' + e[2] + '"]');

      if (!domnode) {
        return;
      };
      if (e[1] == "textContent") {
        domnode.childNodes[0].textContent = domnode["_" + e[1]].replace(new RegExp('\{' + e[0] + '\}', 'mg'), self.state.hasOwnProperty([e[0]]) ? self.state[e[0]] : '');
      } else if (e[1] == "event") {
        if (self.listeners.filter((a) => {
            return a[0] == e[3].slice(2);
          }).length == 0) {
          self.listeners.push([e[3].slice(2), e[2]]);
          domnode.addEventListener(e[3].slice(2), this[e[0]].bind(self));
        }
      } else {
        if (e[3] == "style") {
          for (var j in self.state[e[3]]) {
            domnode[e[3]][j] = self.state[e[3]] ? self.state[e[3]][j] : '';
          }
        } else {
          domnode.setAttribute([e[3]], domnode["_" + e[3]].replace(new RegExp('\{' + e[0] + '\}', 'mg'), self.state.hasOwnProperty([e[0]]) ? self.state[e[0]] : ''));
        }
      }
    })
    return;
  }


}

class TinyComponent {
  constructor() {
    this._id = 0;
    this.hash = "JKLDFI-";
  }

  createComponent(name, obj) {
    let self = this;
    self._id++;
    return new Component(Object.assign({
      Name: name,
      _id: self.hash + self._id
    }, obj))
  }

  append(root, node) {
    let self = this;
    [].concat(node.Nodes).forEach((e) => {
      var tmp = e;
      if (e.Nodes) {
        self.append(e.DOMNode, e);
        root.appendChild(e.DOMNode);
      } else {

        root.appendChild(e);
      }
    });
  }

  render(root_, nodes_) {
    let root;
    let node;
    if (typeof root_ == "string") {
      root = document.querySelector(root_);
    } else {
      root = root_;
    }
    if (typeof nodes_ == "string") {
      node = this.N(nodes_);
    } else {
      node = nodes_;
    };
    console.log(root);
    this.append(root, node);

  }
}

let TinyComp = new TinyComponent();
