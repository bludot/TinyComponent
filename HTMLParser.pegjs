start = expr
expr = tag+

tag
    = whitespace* "<" tag:tagname whitespace* attribute:attribute* whitespace? ">" whitespace* content:(string?) whitespace* children:((tag)*) endtag whitespace* { return {tagName:tag, attributes:attribute || null, textContent:content, children:children }}
    
endtag
    = whitespace* "</" tag:tagname whitespace* attribute:attribute? whitespace? ">" whitespace* { return {tagName:tag}}

attribute
    =  (attr:(field:$((char / "-")+) "=" variable:(quote variable:variable quote " "? {return variable})? value:(quote value:$(!quote .)* quote " "? {return value})? {return {field:field, variable:variable, value:value}}) {return attr})

variable 
    = vari:(("{" vari:$(!"}" .)* "}" {return vari})) {return vari}

quote
    = "\"" / "'"

tagname
    = $(char+)

string
    = first:(char / special / whitespace)* {return first.join("")}

char
    = [a-zA-z0-9]

whitespace
    = ("\n" / " " / "\t")

special
= ("#" / "!" / "{" / "}" / "/" / "=" / "\"" / "-")
